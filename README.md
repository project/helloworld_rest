# Hello World! (REST)

## INTRODUCTION

"Hello World!" example in Drupal.
Drupal 8 module with a REST API.

## REQUIREMENTS

The module requires drupal/rest module as dependency.

## INSTALLATION

Install as you would normally install a Drupal module.

Please download and install through Drupal admin area
or use `composer require drupal/helloworld_rest`,
then enable the "Hello World! (REST)" D8 module.

## CONFIGURATION

The Hello World module does not need any specific
configuration.
